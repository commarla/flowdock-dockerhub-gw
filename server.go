package main

import (
	"flag"
	"fmt"
	"net/http"
	"encoding/json"
	"log"
	"io/ioutil"
	"bytes"
)

var flowtoken string

type Dockerhub struct {
	CallbackURL string `json:"callback_url"`
	PushData struct {
		Images []interface{} `json:"images"`
		PushedAt int `json:"pushed_at"`
		Pusher string `json:"pusher"`
		Tag string `json:"tag"`
	} `json:"push_data"`
	Repository struct {
		CommentCount int `json:"comment_count"`
		DateCreated int `json:"date_created"`
		Name string `json:"name"`
		Namespace string `json:"namespace"`
		Owner string `json:"owner"`
		RepoName string `json:"repo_name"`
		RepoURL string `json:"repo_url"`
		StarCount int `json:"star_count"`
		Status string `json:"status"`
	} `json:"repository"`
}

type Flowdock struct {
	FlowToken string `json:"flow_token"`
	Event string `json:"event"`
	Author struct {
		Name string `json:"name"`
		Avatar string `json:"avatar"`
	} `json:"author"`
	Title string `json:"title"`
	ExternalThreadID string `json:"external_thread_id"`
	Thread struct {
		Title string `json:"title"`
		Fields []struct {
			Label string `json:"label"`
			Value string `json:"value"`
		} `json:"fields"`
		Body string `json:"body"`
		ExternalURL string `json:"external_url"`
		Status struct {
			Color string `json:"color"`
			Value string `json:"value"`
		} `json:"status"`
	} `json:"thread"`
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
	decoder := json.NewDecoder(r.Body)
	var t Dockerhub
	err := decoder.Decode(&t)
	if err != nil {
		log.Println(err.Error())
	}
	sendtoflowdock(t)
}

func sendtoflowdock(hub Dockerhub){
	log.Print(hub)
	var f Flowdock
	f.FlowToken=flowtoken
	f.Author.Name=hub.PushData.Pusher
	f.Title=hub.PushData.Tag
	f.Event="activity"
	f.ExternalThreadID="12345"
	f.Thread.Title="New tag available"
	f.Thread.Body="Body"
	f.Thread.Status.Color="green"
	f.Thread.Status.Value="newTag"
	log.Println(f)

	url := "https://api.flowdock.com/messages"
	fmt.Println("URL:>", url)

	//var jsonStr = []byte(f)
	b, err := json.Marshal(f)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
	panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
}

func main() {
	flag.StringVar(&flowtoken, "flowtoken", "bar", "Flowdock Toke,")
	flag.Parse()
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}
